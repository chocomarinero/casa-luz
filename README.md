# CASA - Luz

Smart lightbulb using Arduino compatible board and Firebase

## Libraries
The following libraries need to be installed through the Arduino IDE before developing or uploading code to the board, **FastLED** can be found through the libraries manager but **Firebase-Arduino** needs to be manually installed, at the time of the development, master branch of the project was downloaded and installed.

- [FastLED](https://github.com/FastLED/FastLED)
- [Firebase-Arduino](https://github.com/FirebaseExtended/firebase-arduino)

As of date, the library **ArduinoJson** (v 5.13.5) by Benoit Blanchon also needs to be installed as it is a dependency of Firebase-Arduino

Basic FastLED functions and effects added following Hans guide:
- [Arduino – All LEDStrip effects in one (NeoPixel and FastLED)](https://www.tweaking4all.com/hardware/arduino/arduino-all-ledstrip-effects-in-one/)

## Board Setup
Current code was written to work with a **NodeMCU v3**, to develop and upload code using the Arduino IDE, make sure to add the following URL to the **"Additional boards manager"** section:
```
http://arduino.esp8266.com/stable/package_esp8266com_index.json
```
Afterwards, search and install **"esp8266" by ESP8266 Community** in the boards manager ande select **"NodeMCU 1.0 (ESP-12E Module)"** to work with the NodeMCU board.

##Schematics
Current setup is using a [NeoPixel Ring with 16 RGBW LEDs](https://www.adafruit.com/product/2855), basic schematics are as follow:

<img src="./schematics/basic.png" alt="drawing" width="500"/>