#include <FastLED.h> //https://github.com/FastLED/FastLED/wiki/Basic-usage

void FadeInOut(byte red, byte green, byte blue){
  float r, g, b;      
  for(int k = 0; k < 256; k=k+1) { 
    r = (k/256.0)*red;
    g = (k/256.0)*green;
    b = (k/256.0)*blue;
    setAll(r,g,b);
    showStrip();
  }
     
  for(int k = 255; k >= 0; k=k-2) {
    r = (k/256.0)*red;
    g = (k/256.0)*green;
    b = (k/256.0)*blue;
    setAll(r,g,b);
    showStrip();
  }
}

void colorWipe(byte red, byte green, byte blue, int SpeedDelay) {
  for(uint16_t i=0; i<NUM_LEDS; i++) {
      setPixel(i, red, green, blue);
      showStrip();
      delay(SpeedDelay);
  }
}

void showStrip() {
   FastLED.show();
}

void setPixel(int Pixel, byte red, byte green, byte blue) {
   leds[Pixel].r = red;
   leds[Pixel].g = green;
   leds[Pixel].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++ ) {
    setPixel(i, red, green, blue); 
  }
  showStrip();
}

void setBrigthness(String b){
  FastLED.setBrightness(b.toInt());
  showStrip();
}

void setColorFromCode(String colorCode){
  long number = (long) strtol( &colorCode[0], NULL, 16 );
  byte r = number >> 16;
  byte g = number >> 8 & 0xFF;
  byte b = number & 0xFF;
  setAll(r,g,b);
}

void setOnOff(String statusString){
  if(statusString == "on"){
    setColorFromCode(DEVICE_COLOR);
  }
  if(statusString == "off"){
    FastLED.clear ();
    showStrip();
  }
}
