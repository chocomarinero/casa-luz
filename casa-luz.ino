#include <FirebaseArduino.h> //https://github.com/FirebaseExtended/firebase-arduino
#include <FastLED.h> //https://github.com/FastLED/FastLED/wiki/Basic-usage
#include <ESP8266WiFi.h>

// LED RING CONFIGURATION
#define DATA_PIN 2
#define NUM_LEDS 16
#define BRIGHTNESS 10
CRGB leds[NUM_LEDS];

// DEVICE CONFIGURATION
#define DEVICE_ID "1"

// WIFI CONFIGURATION
#define WIFI_SSID ""
#define WIFI_PASSWORD ""

// FIREBASE CONFIGURATION
#define FIREBASE_HOST "" // With no http or /
#define FIREBASE_AUTH "" 

// FIREBASE PATHS
#define BASE_PATH "/modules/lights/light_"
#define COLOR_PATH "/status/color"
#define STATUS_PATH "/status/onoff"
#define BRIGHTNESS_PATH "/status/brightness"

// GLOBAL VARIABLES
String DEVICE_STATUS;
String DEVICE_COLOR;
String DEVICE_BRIGHTNESS;

void setup(){
  Serial.begin(9600);

  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
 
  wifiInit();
  firebaseInit();
  dataInit();
  
  Firebase.stream(getPathStream());
}

void loop(){          
  if (Firebase.failed()) {
    Serial.println("streaming error");
    Serial.println(Firebase.error());
  }
  if (Firebase.available()) {
    FirebaseObject event = Firebase.readEvent();
    String eventType = event.getString("type");
    eventType.toLowerCase();
    if(eventType == "put"){
      String data = event.getString("data");
      String path = event.getString("path");
      updateValues(data,path);
    }
  }
}

void updateValues(String newData,String key){
  Serial.print("data: "); Serial.println(newData);
  Serial.print("path: "); Serial.println(key);
  if(newData != ""){
    if(key == STATUS_PATH){
      if(newData != DEVICE_STATUS){
        DEVICE_STATUS = newData;
        setOnOff(DEVICE_STATUS);
      }
    }
    if(key == COLOR_PATH){
      if(newData != DEVICE_COLOR){
        DEVICE_COLOR = newData;
        setColorFromCode(DEVICE_COLOR);
      }    
    }
    if(key == BRIGHTNESS_PATH){
      if(newData != DEVICE_BRIGHTNESS){
        DEVICE_BRIGHTNESS = newData;
        setBrigthness(DEVICE_BRIGHTNESS);
      }    
    }  
    printCurrentValues();    
  }
}

void setInitialValues(){  
  DEVICE_COLOR = Firebase.getString(getPath(COLOR_PATH));
  setColorFromCode(DEVICE_COLOR);
  DEVICE_BRIGHTNESS = Firebase.getString(getPath(BRIGHTNESS_PATH));
  setBrigthness(DEVICE_BRIGHTNESS);
  DEVICE_STATUS = Firebase.getString(getPath(STATUS_PATH));
  setOnOff(DEVICE_STATUS);
}

void printCurrentValues(){
  Serial.println("=== CURRENT VALUES ===");
  Serial.print("STATUS: ");Serial.println(DEVICE_STATUS);
  Serial.print("COLOR: ");Serial.println(DEVICE_COLOR);
  Serial.print("BRIGHTNESS: ");Serial.println(DEVICE_BRIGHTNESS);
}

String getPath(String path){
  String newPath = "";
  newPath += BASE_PATH;
  newPath += DEVICE_ID;
  newPath += path;
  return newPath;
}

String getPathStream(){
  String newPath = "";
  newPath += BASE_PATH;
  newPath += DEVICE_ID;
  return newPath;
}

void wifiInit(){
  WiFi.begin(WIFI_SSID,WIFI_PASSWORD);
  while(WiFi.status() != WL_CONNECTED){
    FadeInOut(0xff, 0xff, 0xff);
    Serial.println("Connecting to wifi...");
    delay(500);
  }
  Serial.println("Wifi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(1000);  
}

void firebaseInit(){
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Serial.println("Connection to Firebase established");
  delay(1000);
}

void dataInit(){
  Serial.println("Getting initial values");
  setInitialValues();
  printCurrentValues();
}
